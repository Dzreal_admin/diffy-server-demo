package com.neo.controller;

import com.alibaba.fastjson.JSONObject;
import com.neo.pojo.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
    @RequestMapping("/")
    public String index() {
        return "Hello Spring Boot 2.0!";
    }

    @GetMapping("/person")
    public String getPerson(){
        Person person = new Person();
        person.setAge(10);
        person.setName("张三");
        person.setHobby(new String[]{"数学","语文","计算机"});
        System.out.println(person);
        return JSONObject.toJSONString(person);
    }
}